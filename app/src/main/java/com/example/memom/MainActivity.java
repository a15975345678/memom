package com.example.memom;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.memom.Fragment.MemoFragment;
import com.example.memom.Fragment.NotificationsFragment;
import com.example.memom.Fragment.SettingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new MemoFragment());
        init_navDrawer();
    }


    private void init_navDrawer() {
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.nav_view);
//        NavigationView navigationView = findViewById(R.id.View_navigation);

        navigation.setSelectedItemId(R.id.navigation_memo);
        // 為navigatin_view設置點擊事件
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // 取得選項id
                int id = item.getItemId();
                Fragment fragment = null;

                // 依照選取的Item開啟對應的頁面
                switch (id) {
                    case R.id.navigation_setting:
                        fragment = new SettingFragment();
                        break;
                    case R.id.navigation_memo:
                        fragment = new MemoFragment();
                        break;
                    case R.id.navigation_notifications:
                        fragment = new NotificationsFragment();
                        break;
                }

                //切換頁面
                loadFragment(fragment);
                return true;
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.Layout_Frame, fragment).commit();
    }
}